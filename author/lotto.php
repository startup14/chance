<?php

$balls = 6;
$from = 52;

if ($argc > 1 && is_numeric($argv[1])) {
    $balls = (int) $argv[1];
}

if ($argc > 2 && is_numeric($argv[2])) {
    $from = (int) $argv[2];
}

$prize = [];
foreach (range(1, $balls) as $lucky_prize) {
    try {
        $ball = random_int(1, $from);
        while (in_array($ball, $prize, true)) {
            $ball = random_int(1, $from);
        }
        $prize[] = $ball;
    } catch (Exception $e) {
        printf("%s\\n", $e->getMessage());
    }
}

printf("Lotto: $balls from $from\n");
try {
    sort($prize);
    printf("%s\n", json_encode($prize, JSON_THROW_ON_ERROR));
} catch (JsonException $e) {
    printf("%s\\n", $e->getMessage());
}